// const BASE_URL = 'https://api.imooc-blog.lgdsunday.club/api';
const BASE_URL = 'http://127.0.0.1:8080';
function request({ url, data, method }) {
  return new Promise((resolve, reject) => {
    // 发起网络请求
    uni.request({
      url: BASE_URL + url,
      data,
      method,
      success: ({ data }) => {
        // 响应成功，获取数据，解析数据
        if (data.success) {
          resolve(data);
        } else {
          // 响应失败，给用户提示
          uni.showToast({
            title: data.message,
            icon: 'none',
            mask: true,
            duration: 3000,
          });
          reject(data.message);
        }
      },
      fail: (error) => {
        reject(error);
      },
      complete: () => {
        // 关闭加载
        uni.hideLoading();
      },
    });
  });
}

export default request;
