import request from '../utils/request';

/**
 * 获取用户基本信息
 */
export function login(data) {
  return request({
    url: '/sys/login',
    method: 'POST',
    data,
  });
}

/**
 * 微信用户授权登录，携带appid和code参数，调用后端接口获取Openid
 */
export function loginAuth(data) {
  return request({
    url: '/wx/user/' + data.appid + '/login/',
    data: {
      code: data.code,
    },
  });
}
